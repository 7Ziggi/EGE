for i in range(1, 10 ** 20):
    n = i
    bin_n = bin(n)[2:]
    if n % 2 == 0:
        bin_n = '1' + bin_n + '0'
    else:
        bin_n = '11' + bin_n + '11'
    r = int(bin_n, 2)
    if r > 52:
        print(i)
        break
