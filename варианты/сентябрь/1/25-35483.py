import time

t0 = time.time()


def D(x):
    d = set()
    for i in range(1, int((x ** 0.5)) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
    return sorted(d)


for i in range(35_000_000, 40_000_001):
    c = 0
    for j in D(i):
        if j % 2 != 0:
            c += 1
        if c > 5:
            break
    if c == 5:
        print(i)

print("Time elapsed: %s seconds" % (time.time() - t0))
# Time elapsed: 2129.4703497886658 seconds
# 35 минут
