for i in range(10 ** 20):
    x = i
    a, b = 0, 0
    while x > 0:
        a += 1
        b += x % 100
        x //= 100
    if a == 2 and b == 13:
        print(i)
        break
