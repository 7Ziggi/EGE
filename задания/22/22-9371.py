for i in range(100, 10 ** 20):
    x = i
    l = x
    m = 65
    if l % 2 == 0:
        m = 52
    while l != m:
        if l > m:
            l -= m
        else:
            m -= l
    if m == 26:
        print(i)
        break
