f = open('txt\\24-27699.txt').readline()
f1 = f
f = f.replace('LDR', '*')
f = f.replace('L', ' ').replace('D', ' ').replace('R', ' ')
f = f.split()
max_len = len(max(f, key=len))
s = 'LDR' * max_len

if s + 'LD' in f1:
    print(max_len * 3 + 2)
elif s + 'L' in f1:
    print(max_len * 3 + 1)
else:
    print(max_len * 3)
