f = open('txt\\24-27695.txt').readline()
c = 1
max_c = -10 ** 20

for i in range(len(f) - 1):
    el1 = f[i]
    el2 = f[i + 1]
    if el1 != el2:
        c += 1
    else:
        max_c = max(max_c, c)
        c = 1
max_c = max(max_c, c)
print(max_c)
