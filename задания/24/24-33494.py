f = open("txt\\24-33494.txt").readline()
alphabet = sorted("QWERTYUIOPASDFGHJKLZXCVBNM")
c_alphabet = [0] * 26
while "E" in f:
    c_alphabet[alphabet.index(f[f.index("E") + 1])] += 1
    f = f.replace("E", "", 1)
print(alphabet[c_alphabet.index(max(c_alphabet))])
