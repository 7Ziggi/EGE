f = open("txt\\24-33526.txt").readline()
alphabet = sorted("QWERTYUIOPASDFGHJKLZXCVBNM")
c_alphabet = [0] * 26
for i in range(1, len(f) - 1):
    pred = f[i - 1]
    tek = f[i]
    sled = f[i + 1]
    if pred == sled:
        c_alphabet[alphabet.index(tek)] += 1

print(alphabet[c_alphabet.index(max(c_alphabet))])
