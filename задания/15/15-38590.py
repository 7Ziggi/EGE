from itertools import combinations


def f(x, a1, a2):
    a = a1 <= x <= a2
    d = 17 <= x <= 58
    c = 29 <= x <= 80
    return d <= (((not c) and (not a)) <= (not d))


Ox = [i / 4 for i in range(16 * 4, 81 * 4 + 1)]
m = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x, a1, a2) for x in Ox):
        m += [a2 - a1]
print(round(min(m)))
