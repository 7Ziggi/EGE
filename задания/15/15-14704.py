def f(a, x, y):
    return ((x < 6) <= (x * x < a)) and ((y * y <= a) <= (y <= 6))


c = 0
for a in range(1000):
    if all(f(a, x, y) for x in range(100) for y in range(100)):
        c += 1
print(c)
