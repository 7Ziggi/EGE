from itertools import *


def f(x, a1, a2):
    p = 5 <= x <= 30
    q = 14 <= x <= 23
    a = a1 <= x <= a2
    return (p == q) <= (not a)


Ox = [i / 4 for i in range(4 * 4, 31 * 4 + 1)]
m = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x, a1, a2) for x in Ox):
        m += [a2 - a1]
print(max(m))
