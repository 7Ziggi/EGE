def f(a, x, y):
    return (y + 2 * x < a) or (x > 30) or (y > 20)


for a in range(1000):
    if all(f(a, x, y) for x in range(100) for y in range(100)):
        print(a)
        break
