for i in range(4, 10 ** 20):  # начинаем от 4, а не от 2
    n = i
    bin_n = bin(n)[2:]

    bin_n = bin_n[:-1]
    bin_n += bin_n[1] + bin_n[1]

    r = int(bin_n, 2)
    if r > 76:
        print(n)
        break
