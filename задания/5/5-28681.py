for i in range(128, 256):
    n = i
    bin_n = bin(n)[2:]
    bin_n = bin_n.replace('1','*')
    bin_n = bin_n.replace('0','1')
    bin_n = bin_n.replace('*','0')
    r = int(bin_n,2)
    n -= r
    if n == 105:
        print(i)
