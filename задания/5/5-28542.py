for i in range(1, 99999999999):
    n = i
    s = ''
    while i > 0:
        s += str(i % 3)
        i //= 3
    s = s[::-1]
    s += str(n % 3)
    r = int(s, 3)
    if r >= 1000:
        print(r)
        break
