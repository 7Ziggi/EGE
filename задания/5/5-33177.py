for i in range (2, 10 ** 20):
    n = i
    bin_n = bin(n)[2:]
    bin_n += bin_n[-2]
    bin_n += bin_n[1]
    r = int(bin_n, 2)
    if r > 150:
        print(i)
        break
