def f(s, c, m):
    if s >= 42:
        return c % 2 == m % 2
    if c == m:
        return 0
    h = [f(s + 1, c + 1, m), f(s + 3, c + 1, m), f(s * 2, c + 1, m)]
    if (c + 1) % 2 == m % 2:
        return any(h)
    else:
        return all(h)


for s in range(1, 42):
    for m in range(1, 5):
        if f(s, 0, m) and m == 4:
            print(s)
            break
