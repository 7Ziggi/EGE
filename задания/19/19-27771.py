def f(s1, s2, c, m):
    if s1 + s2 <= 20:
        return c % 2 == m % 2
    if c == m:
        return 0
    h = [f(s1 - 1, s2, c + 1, m), f(s1, s2 - 1, c + 1, m), f(s1 // 2, s2, c + 1, m), f(s1, s2 // 2, c + 1, m), ]
    if (c + 1) % 2 == m % 2:
        return any(h)
    else:
        return all(h)


for s2 in range(11, 1000):
    for m in range(1, 5):
        if f(10, s2, 0, m):
            if m == 4:
                print(s2)
            break
