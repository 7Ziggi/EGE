def TP4(start, end):
    if start > end or start == 33:
        return 0
    if start == end:
        return 1
    return TP4(start + 1, end) + TP4(start * 2, end)


print(TP4(3, 16) * TP4(16, 37))
