f = open("9-47213.txt").readlines()
c = 0
for i in f:
    l = list(map(int, i.split("\t")))
    d = 0  # кол-во повторяющихся чисел
    p = 0  # число которое повторяется дважды
    sum_np = 0  # сумма не повт чисел
    for h in set(l):
        if l.count(h) == 2:
            p = h
            d += 2
    if d == 2:
        for h in l:
            if h != p:
                sum_np += h
        if sum_np <= p:
            c += 1
print(c)
