from itertools import permutations

w = ['ЙД', 'ЙК', 'ЙС', 'ЙТ', 'ЙР']
c = 0

for i in permutations('ДЕЙКСТРА', r=6):
    s = ''.join(i)
    if s.count('Й') == 1 and any(j in s for j in w):
        c += 1

print(c)
