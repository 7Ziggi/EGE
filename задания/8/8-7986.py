from itertools import *

c = 0
for i in product('ЗИМА', repeat=5):
    s = ''.join(i)
    if (s[0] in 'ЗМ') and (s[-1] in 'ИА'):
        c += 1

print(c)
