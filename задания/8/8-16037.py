from itertools import *

c = 0
for i in product("ЗИМА", repeat=5):
    s = ''.join(i)
    c_g = s.count('И') + s.count('А')
    if c_g == 1:
        c += 1
print(c)
