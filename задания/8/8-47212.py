from itertools import *

iskl = []
c = 0
for i in '1357':
    s1 = '6' + i
    s2 = i + '6'
    iskl += [s1]
    iskl += [s2]

for i in product('01234567', repeat=5):
    s = ''.join(i)
    if s[0] != '0' and s.count('6') == 1 and all(j not in s for j in iskl):
        c += 1

print(c)
