from itertools import *

c = 0
for i in product("ГОД", repeat=6):
    s = ''.join(i)
    if (s[0] in 'ГД') and (s[-1] in 'ГД'):
        c += 1
print(c)
