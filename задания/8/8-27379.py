from itertools import product

g = ['ИО', 'ОИ']
sg = []
c = 0

for i in product('ВКТР', repeat=2):
    s = ''.join(i)
    sg.append(s)

for i in product('ВИКТОР', repeat=4):
    s = ''.join(i)
    if (s.count('В') <= 1) and (s.count('И') <= 1) and (s.count('К') <= 1) and (s.count('Т') <= 1) and (
            s.count('О') <= 1) and (s.count('Р') <= 1):
        if all(not (j in s) for j in sg) and all(not (j in s) for j in g):
            c += 1

print(c)
