from itertools import *

c = 0
for i in product('ГОД', repeat=6):
    s = ''.join(i)
    if s[0] == 'Г' or s[0] == 'Д':
        c += 1

print(c)
