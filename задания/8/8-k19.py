from itertools import product

c = 1
for i in product('АИМРЯ', repeat=4):
    s = ''.join(i)
    if s == 'АРИЯ':
        print(c)
        break
    c += 1
