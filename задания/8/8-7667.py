from itertools import *

c = 0
for i in product('ЕГЭ', repeat=5):
    s = ''.join(i)
    if s[0] == 'Е' or s[0] == 'Э':
        c += 1
print(c)
