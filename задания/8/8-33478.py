from itertools import *

c = 0

iskl = []
for i in permutations('ЙЕ', r=2):
    s = ''.join(i)
    iskl += [s]

for i in product('АНДРЕЙ', repeat=6):
    s = ''.join(i)
    if all(j not in s for j in iskl) and s.count('Й') <= 1 and s[0] != 'Й' and s[-1] != 'Й':
        c += 1
print(c)
