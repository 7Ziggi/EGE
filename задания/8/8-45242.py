from itertools import *

c = 1
for i in product('АБРТЫ', repeat=5):
    s = ''.join(i)
    if (s.count('Ы') == 0) and ('АА' not in s):
        print(c)
        break
    c += 1
