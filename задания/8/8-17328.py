from itertools import *

iskl = []
c = 0

for i in product('ЕАИ', repeat=2):
    s = ''.join(i)
    iskl += [s]

for i in product('ГРСМ', repeat=2):
    s = ''.join(i)
    iskl += [s]

for i in permutations('ГЕРАСИМ', r=7):
    s = ''.join(i)
    if all(j not in s for j in iskl):
        c += 1
print(c)
