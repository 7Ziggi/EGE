def f(x):
    d = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
        if len(d) > 4:
            break
    return sorted(d)


for i in range(185_311, 185_331):
    d = f(i)
    if len(d) == 4:
        print(*d)
