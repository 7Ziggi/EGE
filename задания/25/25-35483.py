import time

t0 = time.time()


def D(x):
    c = 0
    for i in range(1, int((x ** 0.5)) + 1):
        if x % i == 0:
            if i % 2 != 0:
                c += 1
            if (x // i) % 2 != 0:
                c += 1
                if i == (x // i):
                    c -= 1
        if c > 5:
            break
    return c


for i in range(35_000_000, 40_000_001):
    if D(i) == 5:
        print(i)

print("Time elapsed: %s seconds" % (int(time.time() - t0)))
# Time elapsed: 1239.4363272190094 seconds
# 20 минут
