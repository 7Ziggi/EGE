def f(x):
    d = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
    return len(d)


a = []
m = -10 ** 20
for i in range(84052, 84131):
    d = f(i)
    if d > m:
        m = d
        print(d, i)
