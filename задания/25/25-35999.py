a = []
for m in range(0, 100, 2):
    for n in range(1, 100, 2):
        N = (2 ** m) * (3 ** n)
        if 200_000_000 <= N <= 400_000_000:
            a += [N]
print(sorted(a))
