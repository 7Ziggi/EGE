f = [int(i) for i in open('txt\\17-39763.txt')]
k = 0
max_s = -10 ** 20
for i in range(len(f) - 2):
    c = max(f[i], f[i + 1], f[i + 2])
    a = min(f[i], f[i + 1], f[i + 2])
    b = f[i] + f[i + 1] + f[i + 2] - a - c
    if c ** 2 < a ** 2 + b ** 2:
        k += 1
        max_s = max(max_s, f[i] + f[i + 1] + f[i + 2])

print(k, max_s)
