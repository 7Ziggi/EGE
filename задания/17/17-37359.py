f = [int(i) for i in open("txt\\17-37359.txt")]
c = 0
max_sum = - 10 ** 20

for i in range(len(f)):
    for j in range(i + 1, len(f)):
        e1 = f[i]
        e2 = f[j]
        if (e1 + e2) % 117 == 0:
            c += 1
            max_sum = max(max_sum, e1 + e2)

print(c, max_sum)
