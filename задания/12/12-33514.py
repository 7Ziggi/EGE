for i1 in range(1, 101):
    for i2 in range(1, 101):
        for i3 in range(1, 101):
            s = '0' + '1' * i1 + '2' * i2 + '3' * i3
            while ('01' in s) or ('02' in s) or ('03' in s):
                s = s.replace('01', '30', 1)
                s = s.replace('02', '101', 1)
                s = s.replace('03', '202', 1)
            if s.count('1') == 15 and s.count('2') == 10 and s.count('3') == 60:
                print(i1)
                break
