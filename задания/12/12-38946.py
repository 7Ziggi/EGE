min_c = 99999999999999999999999999999
for i in range(200, 1001):
    s = '1' * i
    while ('111' in s) or ('222' in s):
        s = s.replace('111', '22', 1)
        s = s.replace('222', '1', 1)
    if s.count('1') == len(s):
        min_c = min(min_c, i)

print(min_c)
