f = [int(i) for i in open("17.txt")]
c = 0
m_3 = float('inf')
m = float('inf')
a = []
for i in f:
    if (i % 10 == 5) and (len(str(i)) == 3):
        m_3 = min(m_3, i)

for i in range(len(f) - 1):
    if (len(str(f[i])) == 3 and len(str(f[i + 1])) != 3) or (len(str(f[i])) != 3 and len(str(f[i + 1])) == 3):
        if (f[i] + f[i + 1]) % m_3 == 0:
            a.append([f[i], f[i+1]])
            c += 1
            m = min(m, f[i] + f[i + 1])

print(c, m)
print(*a)
