# from itertools import product
#
# f = open('24.txt').readline()
# u = [''.join(i) for i in product("QRS", repeat=2)]
# for i in u:
#     f = f.replace(i, f'{i[0]} {i[1]}')
# f = f.split()
# print(len(max(f, key=len)))

f = open('24.txt').readline()
f = f.replace('Q', '*').replace('R', '*').replace('S', '*')

while '**' in f:
    f = f.replace('**', '* *')
print(max(len(c) for c in f.split()))