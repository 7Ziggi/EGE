def f(x, a):
    return (x & 39 == 0) or ((x & 11 == 0) <= (x & a != 0))


for a in range(1, 1000):
    if all(f(x, a) for x in range(1, 10000)):
        print(a)
        break  # если нужно наиболшее то break убрать
