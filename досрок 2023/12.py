def R(s: str):
    while ('25' in s) or ('355' in s) or ('555' in s):
        if '25' in s:
            s = s.replace('25', '3', 1)
        if '355' in s:
            s = s.replace('355', '52', 1)
        if '555' in s:
            s = s.replace('555', '23', 1)
    return s


n = 4
while True:
    s = '3' + '5' * n
    if sum(map(int, R(s))) == 27:
        print(n)
        break
    n += 1
