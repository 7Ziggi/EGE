with open('26.txt') as f:
    k = int(f.readline())
    n = int(f.readline())
    nums = [[int(x) for x in f.readline().split()] for _ in range(n)]
nums.sort()
times = [0] * (k + 1)
c = last = 0
for st, fn in nums:
    for t in range(1, k + 1):
        # если ячейка уже свободна
        if times[t] < st:
            # запоминаем, когда ячейка освобождается
            times[t] = fn
            c += 1
            last = t
            break
print(c, last)
